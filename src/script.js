const API = "https://ajax.test-danit.com/api/json/";

const containerLoader = document.createElement("div");
containerLoader.classList.add("container-loader");
containerLoader.innerHTML = `
    <div class="configure-border-1">  
        <div class="configure-core"></div>
    </div>  
    <div class="configure-border-2">
        <div class="configure-core"></div>
    </div> 
`;
document.body.append(containerLoader);

function sendRequest(url) {
    return fetch(url);
}

function renderCards() {
    Promise.all([sendRequest(`${API}users`), sendRequest(`${API}posts`)])
        .then((responses) =>
            Promise.all(responses.map((response) => response.json()))
        )
        .then((data) => {
            const [users, posts] = data;

            const containerCards = document.createElement("div");
            containerCards.setAttribute("id", "cards-container");
            addPostModal.after(containerCards);
            containerLoader.style.display = "none";

            posts.forEach((post) => {
                const user = users.find((user) => user.id === post.userId);
                const card = new Card(post, user);
                containerCards.append(card.element);
            });
        })
        .catch((error) => console.error(error));
}
renderCards();

class Card {
    constructor(post, user) {
        this.post = post;
        this.user = user;
        this.element = this.createCard();
    }

    createCard() {
        const card = document.createElement("div");
        card.className = "card";

        const header = document.createElement("div");
        header.className = "post-header";
        header.innerHTML = `<p class="user-name">${
            this.user.name
        }</p><a class="email" href="mailto:${this.user.email}">${this.user.email
            .charAt(0)
            .toLowerCase()}${this.user.email.slice(1)}</a>`;
        card.append(header);

        const postTitle = document.createElement("div");
        postTitle.className = "post-title";
        postTitle.innerHTML = `<h2>${this.post.title
            .charAt(0)
            .toUpperCase()}${this.post.title.slice(1)}</h2>`;

        card.append(postTitle);

        const content = document.createElement("div");
        content.className = "content";
        content.innerHTML = `<p>${this.post.body
            .charAt(0)
            .toUpperCase()}${this.post.body.slice(1)}</p>`;
        card.append(content);

        const deleteButton = document.createElement("button");
        deleteButton.className = "delete-button";
        deleteButton.innerText = "Delete";
        deleteButton.addEventListener("click", () => {
            this.deleteCard();
        });
        card.append(deleteButton);

        return card;
    }

    deleteCard() {
        fetch(`${API}posts/${this.post.id}`, {
            method: "DELETE",
        })
            .then((response) => {
                if (response.ok) {
                    this.element.remove();
                } else {
                    throw new Error("Сard deletion error");
                }
            })
            .catch((error) => console.error(error));
    }
}

const addPostBtn = document.createElement("button");
addPostBtn.setAttribute("id", "add-post-btn");
addPostBtn.innerText = "ADD POST";

const addPostModal = document.createElement("div");
addPostModal.setAttribute("id", "add-post-modal");
addPostModal.insertAdjacentHTML(
    "afterbegin",
    `
    <form>
        <label for="post-title">Title:</label>
        <input type="text" id="post-title" name="post-title" required />

        <label for="post-body">Body:</label>
        <textarea id="post-body" name="post-body" required></textarea>

        <button type="submit">Save</button>
        <button type="button" id="cancel-btn">Cancel</button>
    </form>
    `
);
containerLoader.after(addPostBtn, addPostModal);

const cancelBtn = document.getElementById("cancel-btn");
const containerPosts = document.createElement("div");
containerPosts.setAttribute("id", "posts-container");
addPostModal.after(containerPosts);

let posts = [];

function renderPosts() {
    const containerPosts = document.getElementById("posts-container");
    containerPosts.innerHTML = "";
    console.log(posts);
    posts.forEach((post) => {
        const postElem = document.createElement("article");
        postElem.setAttribute("id", "post");
        postElem.innerHTML = `
    <h2 class="post-title" >${post.title
        .charAt(0)
        .toUpperCase()}${post.title.slice(1)}</h2>
    <p class="post-body" > ${post.body
        .charAt(0)
        .toUpperCase()}${post.body.slice(1)}</p>
    <button class="edit-btn" data-id="${post.id}">Edit</button>
    `;
        containerPosts.append(postElem);
    });
}

function fetchPosts() {
    sendRequest(`${API}posts`)
        .then((response) => response.json())
        .then((data) => {
            posts = data.reverse();
            renderPosts();
        });
}

function addPost(title, body) {
    const data = {
        title,
        body,
        userId: 1,
    };

    fetch(`${API}posts`, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then((data) => {
            posts.unshift(data);
            renderPosts();
            document.getElementById("cards-container").remove();
            addPostModal.style.display = "none";
        });
}

function onAddPostBtnClick() {
    addPostModal.style.display = "block";
}

function onCancelBtnClick() {
    addPostModal.style.display = "none";
}

function onAddPostFormSubmit(event) {
    event.preventDefault();
    const title = document.getElementById("post-title").value;
    const body = document.getElementById("post-body").value;
    addPost(title, body);
}

function onEditBtnClick(event) {
    const postId = event.target.dataset.id;
}

addPostBtn.addEventListener("click", onAddPostBtnClick);
cancelBtn.addEventListener("click", onCancelBtnClick);
addPostModal.addEventListener("submit", onAddPostFormSubmit);
// containerPosts.addEventListener("click", onEditBtnClick);

fetchPosts();
